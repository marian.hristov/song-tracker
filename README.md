# Our song tracker

Important: They are predefined production roles and musicians roles. When the user wants to add a new role in either categories,
the user must be vigilant that they are adding the new role in the appropriate category. Our application does not check
if the user adds the role to the appropriate category.

Ex: Let's say that the guitarist role isn't predefined. When the user wants to add it as a new role, they have to check
that they're adding it to the musician category instead of the production category. The application will not check if
the new guitarist role is in the musician category instead of the production category.

# Limitations

- For each type, there can only be 99999 different instance in total for each type
- Example: Recordings are a type. Only 99999 recordings can be created in total during the lifetime of the database
- A song can only be 9999.9 seconds long
- A recording can only my 9999.9 seconds long
- For the uploader tests, you need to reset the database for each test
- For the downloader tests, you need to have the 3 sample songs on the database
- The GUI needs a few moments to load everything from the database
- Everytime you modify something, you need to make sure to click the "Refresh" button 

# Requirements

- Oracle Database 19C
- Java 16
- Intellij Idea Community Edition 2021.2.3

# Before starting

1. Clone the git repository from https://gitlab.com/marian.hristov/song-tracker into your local machine
2. Make sure to run the sql/install.sql script is run on the database before opening the Java application
3. Enter your database credentials in the <code>src/java/dawson/songtracker/back/dbObjects/DBConnection.java</code> file, it should look like this:
    <code>
    private static String username = "ThisIsMyUsername";
    <br>
    private static String password = "ThisIsMyPassword";
    </code>

# Initial Setup of Intellij Idea Project
1. Open the folder where you cloned the repo in Intellij, by right-clicking on the folder on File Explorer and selecting the "Open Folder as Intellij IDEA Community Edition Project"
2. Make sure of the following:
   - File > Project Structure > Project, make sure Project SDK is 16 and Project language level is 16 too
   - Project is fully loaded
   - The box next to the green play button on the top bar says "MVCApp"
3. Click on the green play button to run the app.

# Uninstall process

1. Make sure the app is not running anymore
2. Delete the database
    - Run uninstall.sql onto your database to remove the database needed for the app
# Assumptions

#### Recordings
- A recording's creation time cannot be modified

#### Compilations
- A compilation's creation time cannot be modified
- A compilation's duration cannot be directly modified. It is dependent on the segment that it uses
- If a compilation is deleted and is also inside a collection, it will be removed from that collection

#### Collections
- Once a collection is distributed, it can no longer be deleted.

# Usage
Different panes are available to the user in order to modify the database. You can modify an item present in a table by double-clicking on it.
